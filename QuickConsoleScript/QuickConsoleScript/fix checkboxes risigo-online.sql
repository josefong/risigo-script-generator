begin transaction

select 
rpa.riskPlanActivityId, rpr.riskPlanRiskId, rpr.relevant, rpa.indActive
into #TempTable 
from RiskPlanActivity rpa 
inner join RiskPlan rp on rp.riskPlanId = rpa.riskPlanId
inner join RiskPlanRisk rpr on rpr.riskPlanId = rp.riskPlanId
inner join RiskArea ra on ra.riskAreaId = rpr.customRiskAreaId
inner join Activity a on rpa.activityId = a.activityId
inner join ActivityArea ar on ar.activityAreaId = a.activityAreaId and ar.name like ('%' +  ra.name + '%')
inner join BranchRisk br on br.branchRiskId = rpr.branchRiskId
inner join ActivityRisk aRisk on aRisk.riskId = br.riskId and aRisk.activityId =a.activityId
where -- rp.locationId =70031 or rp.locationId =70030 and
((case rpr.defaultRelevant when 1 then 1 else null end) is null)

select * from #TempTable

update RiskPlanActivity set indActive =1 where riskPlanActivityId in (select t.riskPlanActivityId from #TempTable t
group by t.riskPlanActivityId, t.indActive
having COUNT(case t.relevant when 1 then 1 else null end)  > 0 )

update #TempTable set indActive =1 where riskPlanActivityId in (select t.riskPlanActivityId from #TempTable t
group by t.riskPlanActivityId, t.indActive
having COUNT(case t.relevant when 1 then 1 else null end)  > 0 )

update rpr set rpr.relevant =1
	from RiskPlanRisk rpr
    where rpr.riskPlanRiskId in (select riskPlanRiskId from #TempTable where indActive =1)

DROP TABLE #TempTable
commit transaction
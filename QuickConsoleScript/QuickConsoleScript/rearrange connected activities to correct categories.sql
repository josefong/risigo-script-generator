begin transaction


(select a.activityId as ActivityId,
 a.activityAreaId as ActivityAreaId,
 r.riskId as RiskId,
 b.branchId as BranchId,
 b.name as BranchName,
  s.name as Sector,
  aa.name as Category ,
   a.name as ActivityName,
    ra.name as RiskArea ,
	r.name as RiskName,
	A.activityAreaId as DesiredActivityAreaId ,
	A.name as DesiredActivityAreaName
	into #temp
from  Activity a
join Branch b on b.branchId = a.branchId 
join Sector s on s.sectorId = b.sectorId
inner join ActivityArea aa on aa.activityAreaId = a.activityAreaId
inner join ActivityRisk ar on ar.activityId = a.activityId
inner join Risk r on r.riskId = ar.riskId
inner join RiskArea ra on r.riskAreaId = ra.riskAreaId
inner join 
	(select aa1.activityAreaId, ra1.riskAreaId ,aa1.name
	from ActivityArea aa1 
	inner join RiskArea ra1 on aa1.name like '%'+ra1.name +'%')
	 A on A.riskAreaId = ra.riskAreaId
where aa.name not like '%'+ra.name +'%')
select * from #temp

INSERT INTO [dbo].[Activity]
           ([name]
           ,[activityAreaId]
           ,[branchId]
           ,[seqNo]
           ,[GUID]
           ,[lastModifiedDate]
           ,[lastModifiedUserId]
           ,[createDate])
     select t.ActivityName,
	 t.DesiredActivityAreaId, 
	-- t.DesiredActivityAreaName,t.Sector, t.Category,
	  t.BranchId,
	 -- t.BranchName,
	   0, NEWID () , GETDATE(),0,GETDATE()
	   from #temp t
	   where not exists
			(select top 1 * from Activity a
				inner join ActivityArea aa on aa.activityAreaId = a.activityAreaId
				inner join Branch b on b.branchId = a.branchId
				where a.name = t.ActivityName and aa.activityAreaId = t.DesiredActivityAreaId and a.branchId = t.BranchId)

delete ar from ActivityRisk ar
  inner join #temp t on t.ActivityId = ar.activityId and t.RiskId = ar.riskId

INSERT INTO [dbo].[ActivityRisk]
           ([activityId]
           ,[riskId]
           ,[lastModifiedDate]
           ,[lastModifiedUserId]
           ,[createDate])
     select a.activityId, t.RiskId, GETDATE(),0, GETDATE() from #temp t
			inner join Activity a on a.activityAreaId = t.DesiredActivityAreaId and a.name = t.ActivityName and a.branchId = t.BranchId
			where not exists (select top 1 * from ActivityRisk ar where ar.activityId = a.activityId and ar.riskId = t.RiskId)
			order by t.ActivityId 

update rpa set rpa.activityId = a.activityId 
	from RiskPlanActivity rpa 
	inner join #temp t on t.ActivityId = rpa.activityId
	inner join Activity a on a.activityAreaId = t.DesiredActivityAreaId and a.name = t.ActivityName and a.branchId = t.BranchId
	
	select * from RiskPlanActivity rpa 
	inner join #temp t on t.ActivityId = rpa.activityId
	inner join Activity a on a.activityAreaId = t.DesiredActivityAreaId and a.name = t.ActivityName and a.branchId = t.BranchId
	order by t.ActivityId asc

	select * 
	from Activity a 
	 inner join #temp t on t.ActivityId = a.activityId
	 inner join ActivityRisk ar on ar.activityId = t.ActivityId and ar.riskId = t.RiskId
delete a from Activity a 
  inner join #temp t on t.ActivityId = a.activityId
  left join ActivityRisk ar on ar.activityId = a.activityId 
  where ar.activityId is null

  --commit 
commit transaction
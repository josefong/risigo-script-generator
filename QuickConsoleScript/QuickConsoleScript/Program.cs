﻿namespace QuickConsoleScript
{
    class ActivityModel
    {
        public string Name { get; set; }
        public string Sector { get; set; }
        public string Branch { get; set; }
        public string Category { get; set; }

        public override string ToString()
        {
            return $"Name:{Name} S:{Sector} B:{Branch} C:{Category}";
        }
    }

    class ActivityModelWithRiskArea
    {
        public string Name { get; set; }
        public string Sector { get; set; }
        public string Branch { get; set; }
        public string Category { get; set; }

        public override string ToString()
        {
            return $"Name:{Name} S:{Sector} B:{Branch} C:{Category}";
        }
    }

    class SuperActivityModel
    {
        public int RiskPlanId { get; set; }
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int SectorId { get; set; }
        public string SectorName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int RiskId { get; set; }
        public string RiskName { get; set; }
        public int ActivityIndActive { get; set; }
        public override string ToString()
        {
            return $"ActivId:{ActivityId} Name: {ActivityName} RiskId: {RiskId}";
         }
    }

    class RiskPlanActivityModel
    {
        public int RiskPlanActivityId { get; set; }
        public int ActivityId { get; set; }
        public int RiskPlanId { get; set; }
        public string ActivityName { get; set; }
        public int Amount { get; set; }
        public int IndActive { get; set; }
    }

    class AcitvityRiskModel
    {
        public int RiskId { get; set; }
        public int ActivityId { get; set; }
    }

    class ResultActivity
    {
        public int RiskPlanId { get; set; }
        public int ActivityId { get; set; }
    }


    class Program
    {
        static void Main(string[] args)
        {
            string saveDestination = @"d:\work\script.sql";
           // string testAllRiskAdmin = @"Data Source=(local);User Id=rizk-web;Password=rizk-web;Initial Catalog=AllRiskAdminDev;app=LINQPad [Query 2];MultipleActiveResultSets=True";
           // string localAllRiskAdmin = @"Data Source=(local);User Id=rizk-web;Password=rizk-web;Initial Catalog=AllRiskAdminDev;app=LINQPad [Query 2];MultipleActiveResultSets=True";
           // string acceptAllRiskAdmin = @"Data Source=s-risigo-acceptance.72media.nl;User Id=rizk-web;Password=rizk-web;Initial Catalog=AllRiskAdmin;app=LINQPad [Query 2];MultipleActiveResultSets=True";
            string acceptAllRiskAdmin = @"Data Source=(local);User Id=rizk-web;Password=rizk-web;Initial Catalog=AllRiskAdmin;app=LINQPad [Query 2];MultipleActiveResultSets=True";
            //  string testAllRisk = @"Data Source=(local);User Id=rizk-web;Password=rizk-web;Initial Catalog=AllRiskDev;app=LINQPad [Query 2];MultipleActiveResultSets=True";
            //  string acceptAllRisk = @"Data Source=s-risigo-acceptance.72media.nl;User Id=rizk-web;Password=rizk-web;Initial Catalog=AllRisk;app=LINQPad [Query 2];MultipleActiveResultSets=True";
            string acceptAllRisk = @"Data Source=(local);User Id=rizk-web;Password=rizk-web;Initial Catalog=AllRisk;app=LINQPad [Query 2];MultipleActiveResultSets=True";

            RemoveDupeActivityOnRisigoAdmin script = new RemoveDupeActivityOnRisigoAdmin();
            script.Execute(acceptAllRiskAdmin);
            var rearrrangeActivitiesFromDifferentCategories = new RearrangeActivitiesFromDifferentCategories();
            rearrrangeActivitiesFromDifferentCategories.Execute(acceptAllRiskAdmin);
            // to be run independently!!!!
           
           

            //ScriptToSetTickDuplicateActivities script2 = new ScriptToSetTickDuplicateActivities();
            //script2.Execute(acceptAllRisk,saveDestination);

        }
    }
}

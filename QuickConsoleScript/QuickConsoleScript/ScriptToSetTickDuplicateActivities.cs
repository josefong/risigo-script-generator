﻿namespace QuickConsoleScript
{
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using Dapper;

    class ScriptToSetTickDuplicateActivities
    {
        public ScriptToSetTickDuplicateActivities()
        {

        }

        public void Execute(string connection, string fileDestination)
        {
            List<string> executes = new List<string>();
            List<ResultActivity> listOfdupes = new List<ResultActivity>();
            string getRiskPlanActivitywithDoubleActivity = @"select rpa.riskPlanId[RiskPlanId] , a.name[ActivityName],
            --rpa.indActive,
            (select count (a.name))[Amount]
            from RiskPlanActivity rpa
            join Activity a on a.activityId = rpa.activityId
            --where rpa.indActive =1
            group by rpa.riskPlanId, a.name--, a.activityId 
            having count (a.name) >1
            order by rpa.riskPlanId,a.name--, a.activityId
            ";

            string getAllRiskPlanActivity = @"select rpa.riskPlanActivityId[RiskPlanActivityId],rpa.riskPlanId[RiskPlanId],a.activityId[ActivityId],rpa.indActive[IndActive],a.name[ActivityName] from RiskPlanActivity rpa
                                                left join Activity a on a.activityId = rpa.activityId
                                                order by rpa.riskPlanId,rpa.activityId ";
            string UpdateRiskPlanActivity = @"Update RiskPlanActivity set indActive = 1 where riskPlanId in ({0}) and activityId in ({1})";
            var sqlconnection = new SqlConnection(connection);
            sqlconnection.Open();
            var q = sqlconnection.CreateCommand();
            q.CommandText = getRiskPlanActivitywithDoubleActivity;
            var reader = sqlconnection.Query<RiskPlanActivityModel>(getRiskPlanActivitywithDoubleActivity).ToList();
            var RiskPlanDB = sqlconnection.Query<RiskPlanActivityModel>(getAllRiskPlanActivity).ToList();

            foreach (var item in reader)
            {
                var result = RiskPlanDB.Select(x => x).Where(x => x.ActivityName == item.ActivityName && x.RiskPlanId == item.RiskPlanId);
                if (result.Any())
                {
                    foreach (var r in result)
                    {
                        if (r.IndActive == 1)
                        {
                            foreach (var r2 in result)
                            {
                                ResultActivity o = new ResultActivity { ActivityId = r2.ActivityId, RiskPlanId = r2.RiskPlanId };
                                listOfdupes.Add(o);
                            }
                            break; // add model and end this loop
                        }
                    }
                }
            }

            sqlconnection.Close();

            foreach (var item in listOfdupes)
            {
                string lol = string.Format(UpdateRiskPlanActivity, item.RiskPlanId, item.ActivityId);
                executes.Add(lol);
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileDestination))
            {
                foreach (var item in executes)
                {
                    file.WriteLine(item);
                }
            }
        }
    }
}
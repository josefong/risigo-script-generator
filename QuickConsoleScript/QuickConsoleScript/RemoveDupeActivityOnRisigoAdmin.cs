﻿namespace QuickConsoleScript
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using Dapper;

    class RemoveDupeActivityOnRisigoAdmin
    {
        public RemoveDupeActivityOnRisigoAdmin()
        {

        }

        public void Execute(string connection)
        {
            List<ActivityModel> lol = new List<ActivityModel>();

            string getAllRiskActivity = @" select activityId [ActivityID], riskId[riskId] from ActivityRisk";
            string deleteActivityRisk = @" delete ActivityRisk where  activityId = {0} and riskId = {1}";

            string superActivityQuery = @"select a.activityId[ActivityId],a.name[ActivityName],a.branchId[BranchId],b.name[BranchName]
                                        ,s.sectorId[SectorId],s.name[SectorName],a.activityAreaId[CategoryId],aa.name[CategoryName],r.riskId[RiskId],r.name [RiskName] 
                    from Activity a
                    left join ActivityArea aa on aa.activityAreaId = a.activityAreaId
                     join Branch b on b.branchId = a.branchId
                     join Sector s on s.sectorId = b.sectorId
                    left join ActivityRisk ar on ar.activityId = a.activityId
                    left join Risk r on r.riskId = ar.riskId
                    order by a.name,b.branchId,aa.activityAreaId";

            string query = @"select a.name[Name],s.name[Sector],b.name[Branch],aa.name[Category],count(*)
                from Activity a
                left join ActivityArea aa on aa.activityAreaId = a.activityAreaId
                 join Branch b on b.branchId = a.branchId
                 join Sector s on s.sectorId = b.sectorId
                left join ActivityRisk ar on ar.activityId = a.activityId
                left join Risk r on r.riskId = ar.riskId
                group by s.name,b.name, a.name,aa.name having count(*)>1";

            string fixQuery = @" update ActivityRisk set activityId = {0} where riskId in ({1}) and activityId in ({2})";
            string getIdQuery = @"select top 1 a.activityId 
            from Activity a
            left join ActivityArea aa on aa.activityAreaId = a.activityAreaId
            join Branch b on b.branchId = a.branchId
            join Sector s on s.sectorId = b.sectorId where a.name = '{0}' AND b.name ='{1}' and s.name = '{2}' and aa.name ='{3}'";

            var sqlconnection = new SqlConnection(connection);
            sqlconnection.Open();
            var q = sqlconnection.CreateCommand();
            q.CommandText = query;
            var reader = sqlconnection.Query<ActivityModel>(query)
                .ToList()
                ;
            var superActivityList = sqlconnection.Query<SuperActivityModel>(superActivityQuery).ToList();

            foreach (var read in reader)
            {
                var duplicateActivities = superActivityList.Select(x => x).Where(x => x.ActivityName == read.Name && x.BranchName == read.Branch && x.SectorName == read.Sector && x.CategoryName == read.Category).ToList();
                Console.WriteLine();

                if (duplicateActivities.Any() && duplicateActivities.GroupBy(x=>x.ActivityId).Count() > 1)
                {
                    
                    var firstActivityRiskId = duplicateActivities.First().ActivityId;
                    var activityToDelete =
                        duplicateActivities
                            .Select(x => x.ActivityId)
                            .Where(x => x != firstActivityRiskId)
                            .Distinct()
                            .ToList();
                    var activityToDeleteSqlArray = string.Join(",", activityToDelete); 
                    var risksToReassign =
                        duplicateActivities.Where(x => x.ActivityId != firstActivityRiskId).Select(x => x.RiskId)
                            .ToList();
                    var risksToReassignSqlArray = string.Join(",", risksToReassign);
                    var reassignScript = $"update ActivityRisk set activityId ={firstActivityRiskId} where riskId in ({risksToReassignSqlArray}) and activityId in ({activityToDeleteSqlArray}) and not exists ( select top 1 * from ActivityRisk where activityId ={firstActivityRiskId} and riskId in ({risksToReassignSqlArray}))";
                    var deleteRemainingActivityRisksScript =
                        $"delete from ActivityRisk where activityId in ({activityToDeleteSqlArray})";
                    var deleteRemainingActivityScript =
                        $"delete from RiskPlanActivity where activityId in ({activityToDeleteSqlArray}) \n"+
                        $"delete from Activity where activityId in ({activityToDeleteSqlArray})";

                    SqlCommand reassignScriptCommand = new SqlCommand(reassignScript, sqlconnection);
                    SqlCommand deleteRemainingActivityRisksScriptCommand = new SqlCommand(deleteRemainingActivityRisksScript, sqlconnection);
                    SqlCommand deleteRemainingActivityScriptCommand = new SqlCommand(deleteRemainingActivityScript, sqlconnection);
                    reassignScriptCommand.ExecuteNonQuery();
                    deleteRemainingActivityRisksScriptCommand.ExecuteNonQuery();
                    deleteRemainingActivityScriptCommand.ExecuteNonQuery();
                }
            }
            sqlconnection.Close();
        }
    }
}